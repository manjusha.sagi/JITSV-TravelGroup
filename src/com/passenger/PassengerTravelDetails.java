package com.passenger;

public class PassengerTravelDetails {
	private String name;
	private int stops;
	private int miles;
	private String passengerType;
	private boolean frequentRiderCard;
	private boolean meal;
	private boolean newspaper;
	
	public PassengerTravelDetails(String name,int stops, int miles, String passengerType,boolean frequentRiderCard, boolean meal, boolean newspaper) {
		this.name = name;
		this.stops = stops;
		this.miles = miles;
		this.passengerType = passengerType;
		this.frequentRiderCard = frequentRiderCard;
		this.meal = meal;
		this.newspaper = newspaper;
		
	}
	public String getPassengerType() {
		return passengerType;
	}
	public void setPassengerType(String passengerType) {
		this.passengerType = passengerType;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStops() {
		return stops;
	}

	

	public int getMiles() {
		return miles;
	}


	public boolean isFrequentRiderCard() {
		return frequentRiderCard;
	}

	public void setFrequentRiderCard(boolean frequentRiderCard) {
		this.frequentRiderCard = frequentRiderCard;
	}

	public boolean isMeal() {
		return meal;
	}

	public void setMeal(boolean meal) {
		this.meal = meal;
	}

	public boolean isNewspaper() {
		return newspaper;
	}

	public void setNewspaper(boolean newspaper) {
		this.newspaper = newspaper;
	}

	
}
