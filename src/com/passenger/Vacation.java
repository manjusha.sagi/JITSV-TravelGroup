package com.passenger;

public class Vacation implements Passenger,Meal {

	@Override
	public double cost(PassengerTravelDetails p) {
		double charge;
		if(p.getMiles()<5||p.getMiles()>4000) {
			throw new IllegalArgumentException("Vacationers cannot travel less than five miles or more than 4000 miles.");
		}else {
		charge=RATE_FACTOR*p.getMiles();
		}
		return charge;
	}

	@Override
	public int Meals(PassengerTravelDetails p) {
		int meals=0;
		if(p.isMeal()) {
			//System.out.println(p.getMiles());
			meals = (int) Math.floor(p.getMiles()/100);
			//System.out.println(meals);
		}
		return meals;
		
	}



}
