package com.passenger;

public class Commuter implements Passenger{

	@Override
	public double cost(PassengerTravelDetails p) {
		double charge;
		charge = RATE_FACTOR * p.getStops();
		if (p.isFrequentRiderCard()) {
			charge = charge - RATE_FACTOR * 0.1 * p.getStops();
		} else {
			return charge;
		}
		return charge;
	}

		
}
