package com.passenger;

public interface Passenger {
	

	double RATE_FACTOR=0.5;
	
	double cost(PassengerTravelDetails p);

}
