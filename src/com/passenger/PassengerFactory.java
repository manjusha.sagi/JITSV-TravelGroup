package com.passenger;


public class PassengerFactory {
	public static Passenger getType(String passengerType) {
		if(passengerType.equalsIgnoreCase("Commuter")) {
			return new Commuter();
		}
		else if(passengerType.equalsIgnoreCase("Vacationer")) {
			return new Vacation();
		}
		
		return null;
		
	}
}
