import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.passenger.Passenger;
import com.passenger.PassengerFactory;
import com.passenger.PassengerTravelDetails;

public class VacationTest {

	private PassengerFactory pf;
	private Passenger p;
	private PassengerTravelDetails ptd;

	@Before
	public void setUp() throws Exception {
		pf = new PassengerFactory();
		p=pf.getType("Vacationer");
	}

	@Test
	public void test() {
		ptd = new PassengerTravelDetails("mno", 0, 90, "Vacationer", false, true, true);
		double expected = 45.0;
		double actual = p.cost(ptd);
		assertEquals(expected, actual,0.01);
	}

}
