import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import com.passenger.PassengerTravelDetails;
import com.passenger.Travel;
import com.passenger.findingEverything;

public class TravelTest {
	private findingEverything t;
	List<PassengerTravelDetails> list1;

	@Before
	public void setUp() throws Exception {
		t = new findingEverything();
		ArrayList<PassengerTravelDetails> list1 = new ArrayList<PassengerTravelDetails>();
		list1.add(new PassengerTravelDetails("abc", 3, 0, "Commuter", true, false, false));
		list1.add(new PassengerTravelDetails("xyz", 5, 0, "Commuter", true, false, false));
		list1.add(new PassengerTravelDetails("def", 4, 0, "Commuter", false, false, true));
		list1.add(new PassengerTravelDetails("pqr", 0, 90, "Vacationer", false, true, false));
		list1.add(new PassengerTravelDetails("mno", 0, 199, "Vacationer", false, true, true));
		t.getDetails(list1);
	}

	@Test
	public void totalNoOfNewsPapers() {
		int expected=2;
		int actual=t.newspapercount;
		System.out.println(actual);
		assertEquals(expected,actual);
	}
	
	@Test
	public void totalNoOfMeals() {                                   
		int expected =1;
		int actual = t.totalmeals;
		assertEquals(expected,actual);
	}
	
	@Test
	public void totalCostTest() {
		double expected =150.1;
		double actual = t.totalcost;
		assertEquals(expected, actual,0.01);
	}

}
