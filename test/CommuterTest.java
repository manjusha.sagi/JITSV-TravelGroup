import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.passenger.Passenger;
import com.passenger.PassengerFactory;
import com.passenger.PassengerTravelDetails;

public class CommuterTest {

	private PassengerFactory pf;
	private Passenger p;
	private PassengerTravelDetails ptd;

	@Before
	public void setUp() throws Exception {
		pf = new PassengerFactory();
		p=pf.getType("Commuter");
	}

	@Test
	public void test1() {
		ptd = new PassengerTravelDetails("abc", 3, 0, "Commuter", true, false, false);
		double expected = 1.35;
		double actual = p.cost(ptd);
		assertEquals(expected, actual,0.01);
	}
	
	@Test
	public void test2() {
		ptd = new PassengerTravelDetails("def", 4, 0, "Commuter", false, false, true);
		double expected = 2.00;
		double actual = p.cost(ptd);
		assertEquals(expected, actual,0.01);
	}

}
